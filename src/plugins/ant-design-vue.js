import Vue from 'vue'
import {
  Breadcrumb,
  Button,
  Carousel,
  Col,
  Icon,
  Input,
  Layout,
  Menu,
  Message,
  Row,
  Select,
  Table,
  Upload
} from 'ant-design-vue'

Vue.use(Breadcrumb)
Vue.use(Button)
Vue.use(Carousel)
Vue.use(Col)
Vue.use(Icon)
Vue.use(Input)
Vue.use(Layout)
Vue.use(Menu)
Vue.use(Message)
Vue.use(Row)
Vue.use(Select)
Vue.use(Table)
Vue.use(Upload)

Object.defineProperty(Vue.prototype, '$message', { value: Message })
