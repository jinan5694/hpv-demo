import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  scrollBehavior () {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/hpv',
      component: () => import('./views/Hpv.vue')
    },
    {
      path: '/hpe',
      component: () => import('./views/Hpe.vue')
    }
  ]
})
