export default {
  main: {
    name: 'Total',
    ytdTarget: 6.95,
    ytdActual: 6.83,
    currentTarget: 6.95,
    currentActual: 6.83,
    yeTarget: 6.5,
    yeFc: 6.5
  },
  machiningE3: [
    {
      name: 'Machining E3',
      ytdTarget: 1.52,
      ytdActual: 1.52,
      currentTarget: 1.52,
      currentActual: 1.52,
      yeTarget: 1.46,
      yeFc: 1.46
    }
  ],
  assemblyE3: [
    {
      name: 'Assembly E3',
      ytdTarget: 2.89,
      ytdActual: 2.89,
      currentTarget: 2.89,
      currentActual: 2.89,
      yeTarget: 2.85,
      yeFc: 2.85
    }
  ],
  supportingAreas: [
    {
      name: 'Supporting areas',
      ytdTarget: 0.11,
      ytdActual: 0.11,
      currentTarget: 0.11,
      currentActual: 0.11,
      yeTarget: 0.12,
      yeFc: 0.12
    }
  ],
  vpOffice: [
    {
      name: 'VP Office',
      ytdTarget: 0.02,
      ytdActual: 0.01,
      currentTarget: 0.02,
      currentActual: 0.01,
      yeTarget: 0.02,
      yeFc: 0.02
    }
  ],
  logistics: [
    {
      name: 'Logistics',
      ytdTarget: 0.23,
      ytdActual: 0.19,
      currentTarget: 0.23,
      currentActual: 0.19,
      yeTarget: 0.19,
      yeFc: 0.18
    }
  ],
  qm: [
    {
      name: 'QM',
      ytdTarget: 0.46,
      ytdActual: 0.41,
      currentTarget: 0.46,
      currentActual: 0.41,
      yeTarget: 0.40,
      yeFc: 0.40
    }
  ],
  testingRework: [
    {
      name: 'Testing & Rework',
      ytdTarget: 0.70,
      ytdActual: 0.70,
      currentTarget: 0.70,
      currentActual: 0.70,
      yeTarget: 0.58,
      yeFc: 0.58
    }
  ],
  integratedTechnology: [
    {
      name: 'Integrated Technology',
      ytdTarget: 0.17,
      ytdActual: 0.15,
      currentTarget: 0.17,
      currentActual: 0.15,
      yeTarget: 0.16,
      yeFc: 0.16
    }
  ],
  plantOperation: [
    {
      name: 'Plant Operation',
      ytdTarget: 0.06,
      ytdActual: 0.03,
      currentTarget: 0.06,
      currentActual: 0.03,
      yeTarget: 0.05,
      yeFc: 0.05
    }
  ],
  maintenance: [
    {
      name: 'Maintenance',
      ytdTarget: 0.66,
      ytdActual: 0.64,
      currentTarget: 0.66,
      currentActual: 0.64,
      yeTarget: 0.56,
      yeFc: 0.56
    }
  ],
  me: [
    {
      name: 'ME',
      ytdTarget: 0.14,
      ytdActual: 0.14,
      currentTarget: 0.14,
      currentActual: 0.14,
      yeTarget: 0.13,
      yeFc: 0.13
    }
  ]
}
