export default {
  main: {
    name: 'Total',
    ytdTarget: 47.3,
    ytdActual: 47.6,
    currentTarget: 46.5,
    currentActual: 46.7,
    yeTarget: 48.2,
    yeFc: 48.2
  },
  biw: [
    {
      name: 'Total',
      ytdTarget: 15.6,
      ytdActual: 16,
      currentTarget: 15.8,
      currentActual: 15.9,
      yeTarget: 15.3,
      yeFc: 15.3
    },
    {
      name: 'MRA I',
      ytdTarget: 14.9,
      ytdActual: 15.2,
      currentTarget: 14.8,
      currentActual: 14.6,
      yeTarget: 14.2,
      yeFc: 14.2
    },
    {
      name: 'MAR II',
      ytdTarget: 14.8,
      ytdActual: 15.5,
      currentTarget: 15.1,
      currentActual: 14.9,
      yeTarget: 15,
      yeFc: 15
    },
    {
      name: 'NGCC',
      ytdTarget: 19.7,
      ytdActual: 19.5,
      currentTarget: 19.6,
      currentActual: 19.7,
      yeTarget: 18,
      yeFc: 18
    }
  ],
  paint: [
    {
      name: 'Total',
      ytdTarget: 6,
      ytdActual: 6.2,
      currentTarget: 5.6,
      currentActual: 5.5,
      yeTarget: 5.6,
      yeFc: 5.6
    },
    {
      name: 'Paint I',
      ytdTarget: 6.6,
      ytdActual: 6.6,
      currentTarget: 6.6,
      currentActual: 6.6,
      yeTarget: 6.3,
      yeFc: 6.3
    },
    {
      name: 'Paint II',
      ytdTarget: 5.3,
      ytdActual: 5.7,
      currentTarget: 5.3,
      currentActual: 5.7,
      yeTarget: 5,
      yeFc: 5
    }
  ],
  assembly: [
    {
      name: 'Total',
      ytdTarget: 14.9,
      ytdActual: 15.2,
      currentTarget: 13.5,
      currentActual: 13.8,
      yeTarget: 14.9,
      yeFc: 14.9
    },
    {
      name: 'MRA I',
      ytdTarget: 14.8,
      ytdActual: 15.2,
      currentTarget: 14.8,
      currentActual: 15.2,
      yeTarget: 15,
      yeFc: 15
    },
    {
      name: 'MAR II',
      ytdTarget: 15.2,
      ytdActual: 15.2,
      currentTarget: 15.3,
      currentActual: 15.2,
      yeTarget: 15.3,
      yeFc: 15.3
    },
    {
      name: 'NGCC',
      ytdTarget: 14.5,
      ytdActual: 14.7,
      currentTarget: 14.5,
      currentActual: 14.7,
      yeTarget: 14,
      yeFc: 14
    }
  ],
  logistics: [
    {
      name: 'Logistics',
      ytdTarget: 4.3,
      ytdActual: 4.2,
      currentTarget: 4.3,
      currentActual: 4.2,
      yeTarget: 3.8,
      yeFc: 3.8
    }
  ],
  maintenance: [
    {
      name: 'Maintenance',
      ytdTarget: 3.6,
      ytdActual: 3.4,
      currentTarget: 3.6,
      currentActual: 3.4,
      yeTarget: 3.5,
      yeFc: 3.5
    }
  ],
  supportingAreas: [
    {
      name: 'Supporting areas',
      ytdTarget: 0.7,
      ytdActual: 0.6,
      currentTarget: 0.7,
      currentActual: 0.6,
      yeTarget: 0.7,
      yeFc: 0.7
    }
  ],
  qm: [
    {
      name: 'QM',
      ytdTarget: 2.3,
      ytdActual: 2.3,
      currentTarget: 2.3,
      currentActual: 2.3,
      yeTarget: 2,
      yeFc: 2
    }
  ],
  me: [
    {
      name: 'ME',
      ytdTarget: 1.1,
      ytdActual: 1.2,
      currentTarget: 1.1,
      currentActual: 1.2,
      yeTarget: 1,
      yeFc: 1
    }
  ]
}
